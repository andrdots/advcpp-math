// SPDX-License-Identifier: MIT-0
/*
 * Authors:
 * * Andrey N. Dotsenko
 *
 * Copyright (c) 2023 Andrey N. Dotsenko
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 * and associated documentation files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom
 * the Software is furnished to do so.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include <advcpp/Matrix.h>

#include <gtest/gtest.h>

using namespace advcpp::in4D;

using ::testing::InitGoogleTest;

TEST(MatrixTest, Constructor)
{
    Matrix m(
        {{{1.0, 0.0, 0.0, 0.0}, {0.0, 1.0, 0.0, 0.0}, {0.0, 0.0, 1.0, 0.0}, {0.0, 0.0, 0.0, 1.0}}});
    ASSERT_EQ(m[0][0], 1.0);
    ASSERT_EQ(m[1][1], 1.0);
    ASSERT_EQ(m[2][2], 1.0);
    ASSERT_EQ(m[3][3], 1.0);
    ASSERT_EQ(m[0][1], 0.0);
    ASSERT_EQ(m[1][0], 0.0);
}

TEST(MatrixTest, Rotation)
{
    Matrix m = Matrix<>::Rotation({0.0, 0.0, 1.0, 1.0}, M_PI / 2);
    Vector v(1.0, 0.0, 0.0);

    Vector<> vs = v * m;
    ASSERT_NEAR(vs.x, 0.0, 0.1);
    ASSERT_NEAR(vs.y, 1.0, 0.1);
    ASSERT_NEAR(vs.z, 0.0, 0.1);
    ASSERT_NEAR(vs.w, 1.0, 0.1);

    vs = vs * m;
    ASSERT_NEAR(vs.x, -1.0, 0.1);
    ASSERT_NEAR(vs.y, 0.0, 0.1);
    ASSERT_NEAR(vs.z, 0.0, 0.1);
    ASSERT_NEAR(vs.w, 1.0, 0.1);

    m = Matrix<>::Rotation({0.0, 0.0, 1.0, 1.0}, M_PI * 4 + M_PI / 2);
    vs = v * m;
    ASSERT_NEAR(vs.x, 0.0, 0.1);
    ASSERT_NEAR(vs.y, 1.0, 0.1);
    ASSERT_NEAR(vs.z, 0.0, 0.1);
    ASSERT_NEAR(vs.w, 1.0, 0.1);
}

TEST(MatrixTest, MatrixProduct)
{
    Matrix m1(
        {{{1.0, 2.0, 3.0, 4.0},
          {5.0, 6.0, 7.0, 8.0},
          {9.0, 10.0, 11.0, 12.0},
          {13.0, 14.0, 15.0, 16.0}}});
    Matrix m2(
        {{{16.0, 15.0, 14.0, 13.0},
          {12.0, 11.0, 10.0, 9.0},
          {8.0, 7.0, 6.0, 5.0},
          {4.0, 3.0, 2.0, 1.0}}});

    Matrix m = m1 * m2;
    ASSERT_EQ(m[0][0], 80);
    ASSERT_EQ(m[0][1], 70);
    ASSERT_EQ(m[0][2], 60);
    ASSERT_EQ(m[0][3], 50);
    ASSERT_EQ(m[1][0], 240);
    ASSERT_EQ(m[1][1], 214);
    ASSERT_EQ(m[1][2], 188);
    ASSERT_EQ(m[1][3], 162);
    ASSERT_EQ(m[2][0], 400);
    ASSERT_EQ(m[2][1], 358);
    ASSERT_EQ(m[2][2], 316);
    ASSERT_EQ(m[2][3], 274);
    ASSERT_EQ(m[3][0], 560);
    ASSERT_EQ(m[3][1], 502);
    ASSERT_EQ(m[3][2], 444);
    ASSERT_EQ(m[3][3], 386);

    m = m1;
    m *= m2;
    ASSERT_EQ(m[0][0], 80);
    ASSERT_EQ(m[0][1], 70);
    ASSERT_EQ(m[0][2], 60);
    ASSERT_EQ(m[0][3], 50);
    ASSERT_EQ(m[1][0], 240);
    ASSERT_EQ(m[1][1], 214);
    ASSERT_EQ(m[1][2], 188);
    ASSERT_EQ(m[1][3], 162);
    ASSERT_EQ(m[2][0], 400);
    ASSERT_EQ(m[2][1], 358);
    ASSERT_EQ(m[2][2], 316);
    ASSERT_EQ(m[2][3], 274);
    ASSERT_EQ(m[3][0], 560);
    ASSERT_EQ(m[3][1], 502);
    ASSERT_EQ(m[3][2], 444);
    ASSERT_EQ(m[3][3], 386);
}

int
main(int argc, char **argv)
{
    InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
