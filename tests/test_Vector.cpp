// SPDX-License-Identifier: MIT-0
/*
 * Authors:
 * * Andrey N. Dotsenko
 *
 * Copyright (c) 2023 Andrey N. Dotsenko
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 * and associated documentation files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom
 * the Software is furnished to do so.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include <advcpp/Vector.h>

#include <gtest/gtest.h>

using namespace advcpp::in4D;

using ::testing::InitGoogleTest;

TEST(VectorTest, Len)
{
    Vector v1(2.0, 1.0, 2.0);

    double len = v1.len();
    ASSERT_EQ(len, 3);

    Vector v2(-2.0, -1.0, -2.0);

    len = v2.len();
    ASSERT_EQ(len, 3);
}

TEST(VectorTest, Normalize)
{
    Vector v1(2.0, 1.0, 2.0);

    ASSERT_EQ(v1.normalized().len(), 1.0);

    v1.normalize();

    double len = v1.len();
    ASSERT_EQ(len, 1);

    Vector v2(-2.0, -1.0, -2.0);

    ASSERT_EQ(v2.normalized().len(), 1.0);

    v2.normalize();

    len = v2.len();
    ASSERT_EQ(len, 1);
}

TEST(VectorTest, CrossProduct)
{
    Vector v1(1.0, 0.0, 0.0);
    Vector v2(0.0, 1.0, 0.0);

    Vector v = v1 * v2;
    ASSERT_EQ(v.x, 0.0);
    ASSERT_EQ(v.y, 0.0);
    ASSERT_EQ(v.z, 1.0);
    ASSERT_EQ(v.w, 1.0);

    Vector v3(5.0, 3.0, 0.0);
    Vector v4(10.0, 0.0, 0.0);

    v = v3 * v4;
    ASSERT_EQ(v.x, 0.0);
    ASSERT_EQ(v.y, 0.0);
    ASSERT_EQ(v.z, -30.0);
    ASSERT_EQ(v.w, 1.0);
}

TEST(VectorTest, DotProduct)
{
    Vector v1(1.0, 0.0, 1.0);
    Vector v2(0.0, 1.0, 0.0);

    double dotProduct = v1.dot(v2);
    ASSERT_EQ(dotProduct, 0.0);

    v1 = {1.0, 1.0, 1.0, 1.0};
    v2 = {-1.0, -2.0, -3.0, 1.0};

    dotProduct = v1.dot(v2);
    ASSERT_EQ(dotProduct, -6.0);
}

TEST(VectorTest, DotValue)
{
    Vector v(1.0, 2.0, 3.0);

    v *= 2;
    ASSERT_EQ(v.x, 2.0);
    ASSERT_EQ(v.y, 4.0);
    ASSERT_EQ(v.z, 6.0);
    ASSERT_EQ(v.w, 1.0);

    Vector v2 = v * 0.5;
    ASSERT_EQ(v2.x, 1.0);
    ASSERT_EQ(v2.y, 2.0);
    ASSERT_EQ(v2.z, 3.0);
    ASSERT_EQ(v2.w, 1.0);
}

TEST(VectorTest, DivValue)
{
    Vector v(4.0, 8.0, 12.0);

    v /= 2;
    ASSERT_EQ(v.x, 2.0);
    ASSERT_EQ(v.y, 4.0);
    ASSERT_EQ(v.z, 6.0);
    ASSERT_EQ(v.w, 1.0);

    Vector v2 = v / 2.0;
    ASSERT_EQ(v2.x, 1.0);
    ASSERT_EQ(v2.y, 2.0);
    ASSERT_EQ(v2.z, 3.0);
    ASSERT_EQ(v2.w, 1.0);
}

TEST(VectorTest, AddValue)
{
    Vector v(1.0, 2.0, 3.0);

    v += 2;
    ASSERT_EQ(v.x, 3.0);
    ASSERT_EQ(v.y, 4.0);
    ASSERT_EQ(v.z, 5.0);
    ASSERT_EQ(v.w, 1.0);

    Vector v2 = v + -2.0;
    ASSERT_EQ(v2.x, 1.0);
    ASSERT_EQ(v2.y, 2.0);
    ASSERT_EQ(v2.z, 3.0);
    ASSERT_EQ(v2.w, 1.0);

    v2 -= 1;
    ASSERT_EQ(v2.x, 0.0);
    ASSERT_EQ(v2.y, 1.0);
    ASSERT_EQ(v2.z, 2.0);
    ASSERT_EQ(v2.w, 1.0);

    v = v2 - -1.0;
    ASSERT_EQ(v.x, 1.0);
    ASSERT_EQ(v.y, 2.0);
    ASSERT_EQ(v.z, 3.0);
    ASSERT_EQ(v.w, 1.0);
}

TEST(VectorTest, AddVector)
{
    Vector v1(1.0, 2.0, 3.0);
    Vector v2(2.0, 3.0, 4.0);

    Vector v = v1 + v2;
    ASSERT_EQ(v.x, 3.0);
    ASSERT_EQ(v.y, 5.0);
    ASSERT_EQ(v.z, 7.0);
    ASSERT_EQ(v.w, 1.0);

    v = v - v2;
    ASSERT_EQ(v.x, 1.0);
    ASSERT_EQ(v.y, 2.0);
    ASSERT_EQ(v.z, 3.0);
    ASSERT_EQ(v.w, 1.0);

    v += v2;
    ASSERT_EQ(v.x, 3.0);
    ASSERT_EQ(v.y, 5.0);
    ASSERT_EQ(v.z, 7.0);
    ASSERT_EQ(v.w, 1.0);

    v -= v2;
    ASSERT_EQ(v.x, 1.0);
    ASSERT_EQ(v.y, 2.0);
    ASSERT_EQ(v.z, 3.0);
    ASSERT_EQ(v.w, 1.0);
}

TEST(VectorTest, ProjLen)
{
    Vector v1(2.0, 2.0, 2.0);
    Vector v2(12.0, 0.0, 0.0);

    double projLen = v1.projLen(v2);
    ASSERT_EQ(projLen, 2.0);

    projLen = v2.projLen(v1);
    ASSERT_NEAR(projLen, 6.928203230275509, 0.001);

    v2 *= -1;

    projLen = v1.projLen(v2);
    ASSERT_EQ(projLen, -2.0);
}

TEST(VectorTest, Proj)
{
    Vector v1(2.0, 2.0, 2.0);
    Vector v2(0.0, 0.0, 12.0);

    Vector vProj = v1.proj(v2);

    ASSERT_EQ(vProj.x, 0.0);
    ASSERT_EQ(vProj.y, 0.0);
    ASSERT_EQ(vProj.z, 2.0);
    ASSERT_EQ(vProj.w, 1.0);

    vProj = v2.proj(v1);
    ASSERT_EQ(vProj.x, 4.0);
    ASSERT_EQ(vProj.y, 4.0);
    ASSERT_EQ(vProj.z, 4.0);
    ASSERT_EQ(vProj.w, 1.0);
}

TEST(VectorTest, ProjToPlane)
{
    Vector v1(2.0, 2.0, 2.0);
    Vector v2(0.0, 0.0, 1.0);

    Vector vProj = v1.projToPlane(v2);

    ASSERT_EQ(vProj.x, 2.0);
    ASSERT_EQ(vProj.y, 2.0);
    ASSERT_EQ(vProj.z, 0.0);
    ASSERT_EQ(vProj.w, 1.0);

    v2 *= -1;
    vProj = v1.projToPlane(v2);

    ASSERT_EQ(vProj.x, 2.0);
    ASSERT_EQ(vProj.y, 2.0);
    ASSERT_EQ(vProj.z, 0.0);
    ASSERT_EQ(vProj.w, 1.0);
}

int
main(int argc, char **argv)
{
    InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
