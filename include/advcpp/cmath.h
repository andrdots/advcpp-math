// SPDX-License-Identifier: LGPL-2.1-or-later
/*
 * Authors:
 * * Andrey N. Dotsenko
 *
 * Copyright (c) 2023 Andrey N. Dotsenko
 */
#ifndef ADVCPP_MATH_CMATH_H_
#define ADVCPP_MATH_CMATH_H_

#include <cmath>

namespace advcpp
{

template<typename T = double>
T
sqrt(T value);

template<typename T = double>
T
sin(T value);

template<typename T = double>
T
cos(T value);

template<typename T = double>
T
fabs(T value);

template<typename T = double>
T
modf(T num, T *iptr);

template<typename T = double>
constexpr T
sqr(T value)
{
    return value * value;
}

template<>
double
sqrt<double>(double value)
{
    return std::sqrt(value);
}

template<>
float
sqrt<float>(float value)
{
    return std::sqrt(value);
}

template<>
double
sin<double>(double value)
{
    return std::sin(value);
}

template<>
float
sin<float>(float value)
{
    return std::sin(value);
}

template<>
double
cos<double>(double value)
{
    return std::cos(value);
}

template<>
float
cos<float>(float value)
{
    return std::cos(value);
}

template<>
double
fabs<double>(double value)
{
    return std::fabs(value);
}

template<>
float
fabs<float>(float value)
{
    return std::fabs(value);
}

template<>
double
modf<double>(double num, double *iptr)
{
    return std::modf(num, iptr);
}

template<>
float
modf<float>(float num, float *iptr)
{
    return std::modf(num, iptr);
}

} // namespace advcpp

#endif // ADVCPP_MATH_CMATH_H_
