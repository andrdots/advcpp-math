// SPDX-License-Identifier: LGPL-2.1-or-later
/*
 * Authors:
 * * Andrey N. Dotsenko
 *
 * Copyright (c) 2023 Andrey N. Dotsenko
 */

#ifndef ADVCPP_MATH_Vector_H_
#define ADVCPP_MATH_Vector_H_

#include "Point.h"
#include "cmath.h"

namespace advcpp
{

namespace in4D
{

template<typename T = double>
struct Vector : Point<T> {
    Vector() : Point<T>() {}
    Vector(T x, T y, T z, T w = 1.0);
    Vector(const Point<T> &start, const Point<T> &end);
    Vector(const Vector<T> &other) : Point<T>(other.x, other.y, other.z, other.w) {}

    Vector<T> &operator=(const Vector<T> &other);
    Vector<T> &operator=(Vector<T> &&other);

    Vector<T> &operator+=(T value);
    Vector<T> &operator-=(T value);

    Vector<T> &operator+=(const Vector<T> &other);
    Vector<T> &operator-=(const Vector<T> &other);

    Vector<T> &operator*=(T value);
    Vector<T> &operator/=(T value);

    T &operator[](size_t rowIndex);
    const T &operator[](size_t rowIndex) const;

    T len() const;

    T dot(const Vector<T> &other);

    T projLen(const Vector<T> &other);
    Vector<T> proj(const Vector<T> &other);
    Vector<T> projToPlane(const Vector<T> &n);

    Vector<T> &normalize();
    Vector<T> normalized();
};

template<typename T>
Vector<T>::Vector(T x, T y, T z, T w) : Point<T>(x, y, z, w)
{
}

template<typename T>
Vector<T>::Vector(const Point<T> &start, const Point<T> &end)
    : Point<T>(end.x - start.x, end.y - start.y, end.z - start.z, 1.0)
{
}

template<typename T>
Vector<T> &
Vector<T>::operator=(const Vector<T> &other)
{
    this->data[0] = other.data[0];
    this->data[1] = other.data[1];
    this->data[2] = other.data[2];
    this->data[3] = other.data[3];
    return *this;
}

template<typename T>
Vector<T> &
Vector<T>::operator=(Vector<T> &&other)
{
    this->data[0] = other.data[0];
    this->data[1] = other.data[1];
    this->data[2] = other.data[2];
    this->data[3] = other.data[3];
    return *this;
}

template<typename T>
Vector<T> &
Vector<T>::operator+=(T value)
{
    this->x += value;
    this->y += value;
    this->z += value;
    return *this;
}

template<typename T>
Vector<T> &
Vector<T>::operator-=(T value)
{
    this->x -= value;
    this->y -= value;
    this->z -= value;
    return *this;
}

template<typename T>
Vector<T> &
Vector<T>::operator+=(const Vector<T> &other)
{
    this->x = this->x / this->w + other.x / other.w;
    this->y = this->y / this->w + other.y / other.w;
    this->z = this->z / this->w + other.z / other.w;
    return *this;
}

template<typename T>
Vector<T> &
Vector<T>::operator-=(const Vector<T> &other)
{
    this->x = this->x / this->w - other.x / other.w;
    this->y = this->y / this->w - other.y / other.w;
    this->z = this->z / this->w - other.z / other.w;
    return *this;
}

template<typename T>
Vector<T> &
Vector<T>::operator*=(T value)
{
    this->x *= value;
    this->y *= value;
    this->z *= value;
    return *this;
}

template<typename T>
Vector<T> &
Vector<T>::operator/=(T value)
{
    this->x /= value;
    this->y /= value;
    this->z /= value;
    return *this;
}

template<typename T>
T &
Vector<T>::operator[](size_t colIndex)
{
    return this->data[colIndex];
}

template<typename T>
const T &
Vector<T>::operator[](size_t colIndex) const
{
    return this->data[colIndex];
}

template<typename T>
T
Vector<T>::len() const
{
    T l = sqrt(this->x * this->x + this->y * this->y + this->z * this->z);
    if (this->w != 0) {
        l = l / this->w;
    }
    return l;
}

template<typename T>
T
Vector<T>::dot(const Vector<T> &other)
{
    return (this->x * other.x + this->y * other.y + this->z * other.z) / (this->w * other.w);
}

template<typename T>
T
Vector<T>::projLen(const Vector<T> &other)
{
    return this->dot(other) / other.len();
}

template<typename T>
Vector<T>
Vector<T>::proj(const Vector<T> &other)
{
    T k = this->dot(other) /
          ((other.x * other.x + other.y * other.y + other.z * other.z) / (other.w * other.w));
    return other * k;
}

template<typename T>
Vector<T>
Vector<T>::projToPlane(const Vector<T> &n)
{
    Vector<T> projToNormal = this->proj(n);
    projToNormal = *this - projToNormal;
    return projToNormal;
}

template<typename T>
Vector<T> &
Vector<T>::normalize()
{
    T len = this->len();
    (*this) /= len;
    return *this;
}

template<typename T>
Vector<T>
Vector<T>::normalized()
{
    T len = this->len();
    return (*this) / len;
}

template<typename T>
Vector<T>
operator-(const Vector<T> &v1, T value)
{
    return Vector<T>(v1.x / v1.w - value, v1.y / v1.w - value, v1.z / v1.w - value);
}

template<typename T>
Vector<T>
operator-(T value, const Vector<T> &v1)
{
    return Vector<T>(value - v1.x / v1.w, value - v1.y / v1.w, value - v1.z / v1.w);
}

template<typename T>
Vector<T>
operator-(const Vector<T> &v1, const Vector<T> &v2)
{
    return Vector<T>(
        v1.x / v1.w - v2.x / v2.w,
        v1.y / v1.w - v2.y / v2.w,
        v1.z / v1.w - v2.z / v2.w);
}

template<typename T>
Vector<T>
operator+(const Vector<T> &v1, T value)
{
    return Vector<T>(v1.x / v1.w + value, v1.y / v1.w + value, v1.z / v1.w + value);
}

template<typename T>
Vector<T>
operator+(T value, const Vector<T> &v1)
{
    return Vector<T>(v1.x / v1.w + value, v1.y / v1.w + value, v1.z / v1.w + value);
}

template<typename T>
Vector<T>
operator+(const Vector<T> &v1, const Vector<T> &v2)
{
    return Vector<T>(
        v1.x / v1.w + v2.x / v2.w,
        v1.y / v1.w + v2.y / v2.w,
        v1.z / v1.w + v2.z / v2.w);
}

template<typename T>
Vector<T>
operator*(const Vector<T> &v1, T value)
{
    return Vector<T>(v1.x * value, v1.y * value, v1.z * value, v1.w);
}

template<typename T>
Vector<T>
operator*(T value, const Vector<T> &v1)
{
    return Vector<T>(v1.x * value, v1.y * value, v1.z * value, v1.w);
}

template<typename T>
Vector<T>
operator/(const Vector<T> &v1, T value)
{
    return Vector<T>(v1.x / value, v1.y / value, v1.z / value, v1.w);
}

template<typename T>
Vector<T>
operator/(T value, const Vector<T> &v1)
{
    return Vector<T>(value / v1.x, value / v1.y, value / v1.z, v1.w);
}

template<typename T>
Vector<T>
operator*(const Vector<T> &v1, const Vector<T> &v2)
{
    return Vector<T>(
        v1.y * v2.z - v1.z * v2.y,
        v1.z * v2.x - v1.x * v2.z,
        v1.x * v2.y - v1.y * v2.x);
}

template<typename T>
Vector<T>
operator-(const Point<T> &p1, const Point<T> &p2)
{
    return Vector<T>(
        p1.x / p1.w - p2.x / p2.w,
        p1.y / p1.w - p2.y / p2.w,
        p1.z / p1.w - p2.z / p2.w);
}

template<typename T>
Point<T>
operator+(const Point<T> &p1, const Vector<T> &p2)
{
    return Point<T>(
        p1.x / p1.w + p2.x / p2.w,
        p1.y / p1.w + p2.y / p2.w,
        p1.z / p1.w + p2.z / p2.w);
}

template<typename T>
Point<T>
operator+(const Vector<T> &p2, const Point<T> &p1)
{
    return Point<T>(
        p1.x / p1.w + p2.x / p2.w,
        p1.y / p1.w + p2.y / p2.w,
        p1.z / p1.w + p2.z / p2.w);
}

} // namespace in4D

} // namespace advcpp

#endif /* ADVCPP_MATH_Vector_H_ */
