// SPDX-License-Identifier: LGPL-2.1-or-later
/*
 * Authors:
 * * Andrey N. Dotsenko
 *
 * Copyright (c) 2023 Andrey N. Dotsenko
 */

#ifndef ADVCPP_MATH_Point_H_
#define ADVCPP_MATH_Point_H_

namespace advcpp
{

namespace in4D
{

template<typename T = double>
struct Point {
    T data[4];
    T &x;
    T &y;
    T &z;
    T &w;
    Point() : data{0, 0, 0, 1.0}, x(data[0]), y(data[1]), z(data[2]), w(data[3]) {}

    Point(const Point<T> &other)
        : data{other.data[0], other.data[1], other.data[2], other.data[3]}, x(data[0]), y(data[1]),
          z(data[2]), w(data[3])
    {
    }

    Point(T x, T y, T z, T w = 1.0);

    Point<T> &operator=(Point<T> &&other);
    Point<T> &operator=(const Point<T> &other);
};

template<typename T>
Point<T>::Point(T x, T y, T z, T w)
    : data{x, y, z, w}, x(data[0]), y(data[1]), z(data[2]), w(data[3])
{
}

template<typename T>
Point<T> &
Point<T>::operator=(Point<T> &&other)
{
    this->x = other.x;
    this->y = other.y;
    this->z = other.z;
    this->w = other.w;
    return *this;
}

template<typename T>
Point<T> &
Point<T>::operator=(const Point<T> &other)
{
    this->x = other.x;
    this->y = other.y;
    this->z = other.z;
    this->w = other.w;
    return *this;
}

} // namespace in4D

} // namespace advcpp

#endif /* ADVCPP_MATH_Point_H_ */
