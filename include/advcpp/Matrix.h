// SPDX-License-Identifier: LGPL-2.1-or-later
/*
 * Authors:
 * * Andrey N. Dotsenko
 *
 * Copyright (c) 2023 Andrey N. Dotsenko
 */

#ifndef ADVCPP_MATH_Matrix_H_
#define ADVCPP_MATH_Matrix_H_

#include "Vector.h"

#include <array>

namespace advcpp
{

namespace in4D
{

template<typename T = double>
class Matrix
{
public:
    std::array<std::array<T, 4>, 4> data;
    Matrix() {}
    Matrix(std::array<std::array<T, 4>, 4> rows);

    Matrix<T> &operator*=(const Matrix<T> &other);

    static Matrix<T> Rotation(const Vector<T> &rotVector, T angle);

    std::array<T, 4> &operator[](size_t rowIndex);
    const std::array<T, 4> &operator[](size_t rowIndex) const;
};

template<typename T>
Matrix<T>::Matrix(std::array<std::array<T, 4>, 4> rows) : data(rows)
{
}

template<typename T>
Matrix<T> &
Matrix<T>::operator*=(const Matrix<T> &other)
{
    Matrix<T> m;
    for (int i = 0; i < 4; ++i) {
        for (int j = 0; j < 4; ++j) {
            m[i][j] = 0;
            for (int k = 0; k < 4; ++k) {
                m[i][j] += (*this)[i][k] * other[k][j];
            }
        }
    }
    *this = m;
    return *this;
}

template<typename T>
std::array<T, 4> &
Matrix<T>::operator[](size_t rowIndex)
{
    return data[rowIndex];
}

template<typename T>
const std::array<T, 4> &
Matrix<T>::operator[](size_t rowIndex) const
{
    return data[rowIndex];
}

template<typename T>
Matrix<T>
Matrix<T>::Rotation(const Vector<T> &rotVector, T angle)
{
    T halfAngle = angle / 2;
    Vector q(
        rotVector.x * sin(halfAngle),
        rotVector.y * sin(halfAngle),
        rotVector.z * sin(halfAngle),
        cos(halfAngle));

    Matrix m(
        {{{1 - 2 * q.y * q.y - 2 * q.z * q.z,
           2 * q.x * q.y - 2 * q.w * q.z,
           2 * q.x * q.z + 2 * q.w * q.y,
           0.0},
          {2 * q.x * q.y + 2 * q.w * q.z,
           1 - 2 * q.x * q.x - 2 * q.z * q.z,
           2 * q.y * q.z - 2 * q.w * q.x,
           0.0},
          {2 * q.x * q.z - 2 * q.w * q.y,
           2 * q.y * q.z + 2 * q.w * q.x,
           1 - 2 * q.x * q.x - 2 * q.y * q.y,
           0.0},
          {0.0, 0.0, 0.0, 1.0}}});

    return m;
}

template<typename T>
Vector<T>
operator*(const Vector<T> &v, const Matrix<T> &m)
{
    Vector<T> r;
    for (int i = 0; i < 4; ++i) {
        r[i] = 0.0;
        for (int j = 0; j < 4; ++j) {
            r[i] += m[i][j] * v[j];
        }
    }
    return r;
}

template<typename T>
Matrix<T>
operator*(const Matrix<T> &m1, const Matrix<T> &m2)
{
    Matrix<T> m;
    for (int i = 0; i < 4; ++i) {
        for (int j = 0; j < 4; ++j) {
            m[i][j] = 0;
            for (int k = 0; k < 4; ++k) {
                m[i][j] += m1[i][k] * m2[k][j];
            }
        }
    }
    return m;
}

} // namespace in4D

} // namespace advcpp

#endif /* ADVCPP_MATH_Matrix_H_ */
