# advcpp-math

## Name
Full name: Advanced C++ Math library.

Short name: advcpp-math.

## Description
The library provides advanced math primitives and operations including vectors, matrices and operations under them.

## License
The project is licensed under the GNU Lesser General Public License, version 2.1 or later.
