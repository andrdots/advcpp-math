cmake_minimum_required(VERSION 3.5)
enable_language(CXX)

include(cmake/AdvCMake.cmake)

project(advcpp-math
DESCRIPTION
    "Library with common math primitives and operations"
LANGUAGES
    CXX
)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wall -Werror")

include(FetchContent)
FetchContent_Declare(
    googletest
SOURCE_DIR
    "${CMAKE_CURRENT_SOURCE_DIR}/googletest"
)
FetchContent_MakeAvailable(googletest)

set(COMMON_PROJECT_NAME advcpp)

declare_shared_library(advcpp-math)

target_include_directories(advcpp-math
PUBLIC
    "${CMAKE_CURRENT_SOURCE_DIR}/include"
)

target_sources(advcpp-math
PRIVATE
    "${CMAKE_CURRENT_SOURCE_DIR}/advcpp_math.cpp"
PUBLIC
    "${CMAKE_CURRENT_SOURCE_DIR}/include/${COMMON_PROJECT_NAME}/cmath.h"
    "${CMAKE_CURRENT_SOURCE_DIR}/include/${COMMON_PROJECT_NAME}/Point.h"
    "${CMAKE_CURRENT_SOURCE_DIR}/include/${COMMON_PROJECT_NAME}/Vector.h"
    "${CMAKE_CURRENT_SOURCE_DIR}/include/${COMMON_PROJECT_NAME}/Matrix.h"
)

protect_target(advcpp-math)

install(TARGETS
    advcpp-math
LIBRARY
DESTINATION
    ${CMAKE_INSTALL_LIBDIR}
)

add_subdirectory(tests)
